Questions:

TestList:

Q: Also try with a LinkedList - does it make any difference behaviorally? (ignore performance)

A: No, it did not make a difference behaviorly. I was able to initialize a Linked List in place of an ArrayList,
and the tests all passed.

Q: What does this method do? list.remove(5);
A: it removes the value at index 5

Q: What does this one do? list.remove(Integer.valueOf(5));
A: it removes the value in the list that has an Integer value 5

Test Performance:

run test and record running times for SIZE = 10, 100, 1000, 10000, ...

10:
ArrayListAccess: 17ms
ArrayListAddRemove: 31ms

LinkedListAccess: 16ms
LinkedListAddRemove: 30ms

total: 94ms

100:
ArrayListAccess: 16ms
ArrayListAddRemove: 40ms

LinkedListAccess: 30ms
LinkedListAddRemove: 37ms

total: 123ms

1000:
ArrayListAccess: 19ms
ArrayListAddRemove: 182ms

LinkedListAccess: 348ms
LinkedListAddRemove: 35ms

total: 584ms

10000:
ArrayListAccess: 20 ms
ArrayListAddRemove: 1s 579ms

LinkedListAccess: 4s 106ms
LinkedListAddRemove: 37ms

total: 5s 742ms

100000:
ArrayListAccess: 25 ms
ArrayListAddRemove: 20s 376ms

LinkedListAccess: 52s 966ms
LinkedListAddRemove: 47ms

total: 1m 13s 414ms

As the size increases, the ArrayListAccess method performs a lot faster than the LinkedListAccess method.
However, the LinkedListAddRemove method performs a lot faster than the ArrayListAddRemove method as the size increases.